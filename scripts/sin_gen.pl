#! /usr/bin/perl
use warnings;
use Math::BigFloat qw/bpi/;
use Math::BigInt;

Math::BigFloat->accuracy(200);

print "const long long __sin_reduction_table[64]={\n";
for $i (0..63){
	$x = Math::BigFloat->new('2');
	$y = Math::BigFloat->new($i);
	$z = Math::BigFloat->new('0');
	$z = ((($x**$y - (($x**$y/bpi(200)/2)->bfloor())*bpi(200)*2)*2**60));
	
	$t = ($z->numify());
	$exp = 0;
	while ($t*2 < 2**63){
		$t = $t*2;
		$exp = $exp+1;
	}
	push @e, $exp;

	print "\t";
	$z = ((($x**$y - (($x**$y/bpi(200)/2)->bfloor())*bpi(200)*2)*(2**(60))*(2**$exp)));
	print $z->numify();
	print "ULL,\n";
	print "//".($x**$y - (($x**$y/bpi(200)/2)->bfloor())*bpi(200)*2)."\n"; 		
}
print "};\n\n";

print "const long long __sin_reduction_2PI = ". (((bpi(100)*2**61)->bfloor())->numify()). "ULL;\n";
print "const long __sin_reduction_2PI_exp = -60;\n";

print "const unsigned char __sin_reduction_exp[64]={\n";
for $x (@e){
	print "\t".$x.",\n";
}
print "};\n\n";


